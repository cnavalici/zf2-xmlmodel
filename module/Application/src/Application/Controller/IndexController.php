<?php
/**
 * Zend Framework (http://framework.zend.com/)
 *
 * @link      http://github.com/zendframework/ZendSkeletonApplication for the canonical source repository
 * @copyright Copyright (c) 2005-2014 Zend Technologies USA Inc. (http://www.zend.com)
 * @license   http://framework.zend.com/license/new-bsd New BSD License
 */

namespace Application\Controller;

use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\ViewModel;

use Utils\View\Model\XmlModel;

class IndexController extends AbstractActionController
{
    public function indexAction()
    {
        $books = array("books" => array(
            '@attributes' => array(
                'type' => 'fiction',
                'year' => 2011,
                'bestsellers' => true
            ),
            'book'=> array('1984','Foundation','Stranger in a Strange Land')
        ));

        return new XmlModel($books);
        # return new ViewModel();
    }
}
