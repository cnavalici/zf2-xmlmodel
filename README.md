XmlModel Tutorial
=======================

Introduction
------------
This is the code related to the tutorial found at [here](http://goodies.lemonsoftware.eu/php:zf_custom_xmlmode)


This tutorial is intended to help newcomers to the (in)famous Zend Framework 2 to put some light into custom ViewModel
topic and also custom Strategy. It's maybe already known to the user that the ZF2 documentation is basically a *joke*, and secondly,
in their attempt to make it easier, more flexible and to implement good practices,
the ZF2 actually manages to make things extremely complicated in the end.

Oddly enough, ZF2 offers a JsonModel or a FeedModel, but no XmlModel. The purpose of that tutorial is to show you how.


Contact
------------
Cristian Năvălici lemonsoftware at runbox com
